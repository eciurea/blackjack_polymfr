#pragma once
#include <unordered_map>
#include "Card.h"
#include <string>
#include <SFML/Graphics.hpp>

class TextureHolder {
public:
	void loadTextures(const std::string & path);
private:
	std::unordered_map <Card, sf::Texture> cardToTexture;

};
