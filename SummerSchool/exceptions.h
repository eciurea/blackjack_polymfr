#pragma once
#include <string>

#include <stdexcept>

class FileNotFoundException : public std::invalid_argument
{

public:
	explicit FileNotFoundException(const char* msg);
};

class InvalidSettingValue
{
public:
	InvalidSettingValue(std::string msg);
private: 
	std::string m_msg;
};

