#pragma once

#include "IValidator.h"

#include <regex>


class InputValidator : public IValidator
{
public:
	InputValidator(const std::regex& validationRegex);

	bool isValid(const std::string& input, int maximumAmount = 0) const override;

private:
	std::regex _validationRegex;
};
