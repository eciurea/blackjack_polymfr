#pragma once

#include "Player.h"

class DumbPlayer : public Player
{
public:
	DumbPlayer(const char* name ,int money);
	Action GetAction() const override ;
	~DumbPlayer();
};

