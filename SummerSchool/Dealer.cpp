#include "stdafx.h"
#include "Dealer.h"
#include "Card.h"
#include "Player.h"

#include <algorithm>
#include <ctime>
#include <cstdlib>




Dealer::Dealer(const char* name , int money): Player(name , money)
{
}

Dealer::~Dealer()
{
}

Action Dealer::GetAction() const
{
	Action returnValue = STAND;
	if (m_hand.GetValue() < 16)
		returnValue = HIT;
	return returnValue;
	
}
