#pragma once
#include "Deck.h"
#include "Shoe.h"

#include <vector>
#include <iostream>

#include "Player.h"
class Player;
class Dealer : public Player
{
public:
	Dealer(const char* name, int money);
	~Dealer();
	Action GetAction() const override;
};
