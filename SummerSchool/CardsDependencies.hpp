#pragma once
#include<iostream>
#include <unordered_map>
#include <string>
#include<memory>

#include "Card.h"

namespace cards_dependencies
{
	std::unordered_map<std::string,std::unique_ptr<Card>> fileNameToCard = { {std::string("102.gif") ,std::make_unique<Card>(Suit::SPADES,'2')} };
	
	void x()
	{
		fileNameToCard.insert(std::make_pair(std::string("102.gif"), std::make_unique<Card>(Suit::SPADES, '2')));
	}
	/*{
		{"102.gif",Card(Suit::SPADES,'2')},
		{"103.gif",Card(Suit::SPADES,'3')},
		{"104.gif",Card(Suit::SPADES,'4')},
		{"105.gif",Card(Suit::SPADES,'5')},
		{"106.gif",Card(Suit::SPADES,'6')},
		{"107.gif",Card(Suit::SPADES,'7')},
		{"108.gif",Card(Suit::SPADES,'8')},
		{"109.gif",Card(Suit::SPADES,'9')},
		{"110.gif",Card(Suit::SPADES,'10')},
		{"111.gif",Card(Suit::SPADES,'11')},
		{"112.gif",Card(Suit::SPADES,'12')},
		{"113.gif",Card(Suit::SPADES,'13')},
		{"114.gif",Card(Suit::SPADES,'14')},

		{"202.gif",Card(Suit::HEARTS,'2')},
		{"203.gif",Card(Suit::HEARTS,'3')},
		{"204.gif",Card(Suit::HEARTS,'4')},
		{"205.gif",Card(Suit::HEARTS,'5')},
		{"206.gif",Card(Suit::HEARTS,'6')},
		{"207.gif",Card(Suit::HEARTS,'7')},
		{"208.gif",Card(Suit::HEARTS,'8')},
		{"209.gif",Card(Suit::HEARTS,'9')},
		{"210.gif",Card(Suit::HEARTS,'10')},
		{"211.gif",Card(Suit::HEARTS,'11')},
		{"212.gif",Card(Suit::HEARTS,'12')},
		{"213.gif",Card(Suit::HEARTS,'13')},
		{"214.gif",Card(Suit::HEARTS,'14')},

		{"302.gif",Card(Suit::DIAMONDS,'2')},
		{"303.gif",Card(Suit::DIAMONDS,'3')},
		{"304.gif",Card(Suit::DIAMONDS,'4')},
		{"305.gif",Card(Suit::DIAMONDS,'5')},
		{"306.gif",Card(Suit::DIAMONDS,'6')},
		{"307.gif",Card(Suit::DIAMONDS,'7')},
		{"308.gif",Card(Suit::DIAMONDS,'8')},
		{"309.gif",Card(Suit::DIAMONDS,'9')},
		{"310.gif",Card(Suit::DIAMONDS,'10')},
		{"311.gif",Card(Suit::DIAMONDS,'11')},
		{"312.gif",Card(Suit::DIAMONDS,'12')},
		{"313.gif",Card(Suit::DIAMONDS,'13')},
		{"314.gif",Card(Suit::DIAMONDS,'14')},

		{"402.gif",Card(Suit::CLUBS,'2')},
		{"403.gif",Card(Suit::CLUBS,'3')},
		{"404.gif",Card(Suit::CLUBS,'4')},
		{"405.gif",Card(Suit::CLUBS,'5')},
		{"406.gif",Card(Suit::CLUBS,'6')},
		{"407.gif",Card(Suit::CLUBS,'7')},
		{"408.gif",Card(Suit::CLUBS,'8')},
		{"409.gif",Card(Suit::CLUBS,'9')},
		{"410.gif",Card(Suit::CLUBS,'10')},
		{"411.gif",Card(Suit::CLUBS,'11')},
		{"412.gif",Card(Suit::CLUBS,'12')},
		{"413.gif",Card(Suit::CLUBS,'13')},
		{"414.gif",Card(Suit::CLUBS,'14')}
	};*/


	
}