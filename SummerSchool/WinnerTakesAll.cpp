#include "stdafx.h"
#include "WinnerTakesAll.h"
#include "Player.h"

#include <algorithm>

WinnerTakesAll::WinnerTakesAll(Player ** players, size_t playersCount, const std::vector<int>& bets) 
	: m_players(players),
	m_playersCount(playersCount),
	m_bets(bets)
{
	
}

void WinnerTakesAll::GivePrizes() const
{
	static constexpr int kBlackjackMaximumValue = 21;
	std::sort(m_players, m_players + m_playersCount,
		[&]( Player* lhs,  Player* rhs) -> bool
	{
		return std::abs(static_cast<int>(lhs->GetHand().GetValue() - kBlackjackMaximumValue)) <
			std::abs(static_cast<int>(rhs->GetHand().GetValue() - kBlackjackMaximumValue));
	});

	auto maximumValue = m_players[0]->GetHand().GetValue();
	for (auto index = 0; index < m_playersCount; ++index)
	{
		if (m_players[index]->GetHand().GetValue() == maximumValue)
		{
			if (m_players[index]->GetName() == "Dealer")
			{
				continue;
			}

			m_players[index]->AddMoney(m_bets[index]);
		}
		else
		{
			m_players[index]->SubstractMoney(m_bets[index]);
		}
	}
}
