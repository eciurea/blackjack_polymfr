#include "stdafx.h"
#include "DumbPlayer.h"


DumbPlayer::DumbPlayer(const char * name , int money): Player(name , money)
{}

Action DumbPlayer::GetAction() const
{
	return (m_hand.GetValue() < 21 )? Action::HIT : Action::STAND;
}

DumbPlayer::~DumbPlayer()
{}
