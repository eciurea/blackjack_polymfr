#include "stdafx.h"
#include "InputValidator.h"


InputValidator::InputValidator(const std::regex& validationRegex)
	: _validationRegex(validationRegex)
{
}

bool InputValidator::isValid(const std::string& input, int maximumAmount) const
{
	if (!std::regex_match(input, _validationRegex))
		return false;

	//auto integerValue = std::stoi(input);
	//if (integerValue > maximumAmount)
	//	return false;

	return true;
}
