#pragma once

class IValidator;

class IInputProvider
{
public:
	virtual int requestInput() = 0;
	virtual ~IInputProvider() = default;
};

