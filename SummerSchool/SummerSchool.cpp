// SummerSchool.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "Player.h"
#include "Game.h"
#include "NormalPlayer.h"
#include "DumbPlayer.h"
#include "InputValidator.h"
#include "ConsoleInputProvider.h"
#include "WinnerTakesAll.h"
#include "Configuration.h"
#include "exceptions.h" 

#include <string>
#include <iostream>
#include <SFML/Graphics.hpp>


int main()
{
	/*
	Configuration configuration(std::regex("^ *([0-9_a-zA-Z]*) *= *([1-9]+)[# a-zA-Z0-9]*$"));
	try
	{
		configuration.Load("config.ini");
		auto x = configuration[Configuration::Setting::MaximumPlayers];
		std::cout << configuration;
	}
	catch (FileNotFoundException e)
	{
		std::cout << e.what() << '\n';
	}
	catch (std::string e)
	{
		std::cout << e << '\n';            //aici am ramas
	}*/
	
	Player* players[] =
	{
		new NormalPlayer("Adrian Deaconu(N)" , 100),
		new DumbPlayer("Ichim Sebastian(D)" , 100),
		new NormalPlayer("Bocu Dorin(N)" ,100)
	};
	size_t numPlayers = sizeof(players) / sizeof(Player*);

	IValidator* validator = new InputValidator(std::regex("^[1-9]{1}[0-9]*$"));
	IInputProvider* provider = new ConsoleInputProvider(validator);
	IWinPolicy* winPolicy = new WinnerTakesAll();

	Game game(numPlayers, players , provider , validator, winPolicy);

	game.Play();

	//for (Player* player : players)
	//{
	//	delete player;
	//	player = nullptr;
	//}
	
    return 0;
}