#pragma once
class IWinPolicy
{
public:
	virtual void GivePrizes() const = 0;
	virtual ~IWinPolicy() = default;
};

