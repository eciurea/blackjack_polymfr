#include "stdafx.h"
#include "Player.h"
#include <cstring>

Player::Player(const char * name) : m_name(nullptr), m_hand()
{
	m_name = new char[strlen(name) + 1]; // add 1 for '\0'
	strcpy(m_name, name);
}

Player::Player(const char * name, int currentAmountOfMoney)
	: Player(name)
{
	m_currentAmountOfMoney = currentAmountOfMoney;
}


Player::~Player()
{
	delete[] m_name;
	m_name = nullptr;
}

const char * Player::GetName() const
{
	return m_name;
}

Hand & Player::GetHand()
{
	return m_hand;
}

int Player::GetCurrentAmountOfMoney() const
{
	return m_currentAmountOfMoney;
}

void Player::AddMoney(int moneyToAdd)
{
	m_currentAmountOfMoney += moneyToAdd;
}

void Player::SubstractMoney(int moneyToSubstract)
{
	m_currentAmountOfMoney -= moneyToSubstract;
}

//Action Player::GetAction() const
//{
//	Action returnValue = STAND;
//	if (m_hand.GetValue() < 17) returnValue = HIT;
//
//	return returnValue;
//}
