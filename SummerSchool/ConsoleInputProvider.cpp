#include "stdafx.h"
#include "ConsoleInputProvider.h"

#include <iostream>
#include <string>
#include "InputValidator.h"

ConsoleInputProvider::ConsoleInputProvider(IValidator* validator): m_validator(validator)
{}

int ConsoleInputProvider::requestInput() 
{
	std::string input;
	int value;
	std::cout << " Place a bet : ";
	std::getline(std::cin, input);

	if (!m_validator->isValid(input))
	{
		std::cerr << "\nInput not correct!\n";
		requestInput();
	}
	else
		return std::stoi(input);
}
