#pragma once

#include <string>
class IValidator
{
public :
	virtual bool isValid(const std::string& input, int maximumAmount = 0) const = 0;
	virtual ~IValidator() = default;
};
