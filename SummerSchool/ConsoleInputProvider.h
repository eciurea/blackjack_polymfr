#pragma once
#include "IInputProvider.h"
class IValidator;

class ConsoleInputProvider: public IInputProvider
{
public:
	ConsoleInputProvider(IValidator* validator);
	int requestInput() override;

private :
	IValidator* m_validator;
};

