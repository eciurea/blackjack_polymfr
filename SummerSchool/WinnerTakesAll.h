#pragma once
#include "IWinPolicy.h"

#include <vector>

class Player;

class WinnerTakesAll : public IWinPolicy
{
public:
	WinnerTakesAll()= default;
	WinnerTakesAll(Player** players, size_t playersCount, const std::vector<int>& bets);
	void GivePrizes() const override;

private:
	Player** m_players;
	size_t m_playersCount;
	std::vector<int>  m_bets;
};

