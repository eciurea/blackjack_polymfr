#include "stdafx.h"
#include "Game.h"

#include "Player.h"
#include "DumbPlayer.h"
#include "Dealer.h"
#include "NormalPlayer.h"
#include "WinnerTakesAll.h"

#include <algorithm>
#include <chrono>
#include <random>



Game::Game(size_t numPlayers, Player ** players) :
	m_numPlayers(0),
	m_players(nullptr)
{
	size_t numTotalPlayers = numPlayers + 1; // add the dealer player
	m_players = new Player*[numTotalPlayers];

	for (size_t idx = 0; idx < numPlayers; ++idx)
	{
		m_players[idx] = players[idx];
	}

	m_players[numPlayers] = new Dealer("Dealer" , 100);
	m_numPlayers = numTotalPlayers;
}

Game::Game(size_t numPlayers, Player ** players, IInputProvider * provider, IValidator * validator, IWinPolicy* winPolicy)
	: Game(numPlayers, players)
{
	m_provider = provider;
	m_validator = validator;
	m_winPolicy = winPolicy;
}

Game::~Game()
{
	delete m_players[m_numPlayers - 1];
	delete[] m_players;
	m_players = nullptr;
}

void Game::Play()
{
	if (m_numPlayers < 2) return;

	Shuffle();

	DealInitialCards();

	//test
	PrintPlayers();
	RequestBets();

	m_winPolicy = new WinnerTakesAll(m_players, m_numPlayers, m_bets);

	GameLoop();
}

void Game::DealInitialCards()
{
	for (size_t idx = 0; idx < m_numPlayers; ++idx)
	{
		Deal(m_players[idx]);
		Deal(m_players[idx]);
	}
}

void Game::GameLoop()
{
	bool allStand = false;
	bool* playerStands = new bool[m_numPlayers];
	std::fill(playerStands, playerStands + m_numPlayers, false);

	while (!allStand)
	{
		allStand = true;
		for (size_t idx = 0; idx < m_numPlayers; ++idx)
		{
			if (playerStands[idx]) continue;

			Player* player = m_players[idx];
			Action action = player->GetAction();
			if (action == HIT)
			{
				allStand = false;
				Deal(player);
				printf("%s Hits!\n", player->GetName());
			}
			else
			{
				playerStands[idx] = true;
				printf("%s Stands!\n", player->GetName());
			}
		}

		m_winPolicy->GivePrizes();

		std::cout << "Final stats \n";
		PrintPlayers();

	}



	delete[] playerStands;
	playerStands = nullptr;
}

void Game::PrintPlayers() const
{
	for (size_t idx = 0; idx < m_numPlayers; ++idx)
	{
		Player* player = m_players[idx];
		printf("Name: %s\n", player->GetName());

		player->GetHand().Print();

		printf("%d \n" , player->GetCurrentAmountOfMoney());
	}
}

void Game::Shuffle()
{
	size_t numCards = m_deck.GetNumberOfCards();
	Card** cards = m_deck.GetCards();
	static unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	static std::default_random_engine e(seed);

	std::shuffle(cards, cards + numCards, e);

	m_shoe.SetCards(numCards, cards);
}

void Game::Deal(Player * player)
{
	Card* card = m_shoe.NextCard();
	if (card == nullptr) return;

	player->GetHand().AddCard(card);
}

void Game::RequestBets()
{
	for (int index = 0; index < m_numPlayers; ++index)
	{
		std::cout << m_players[index]->GetName();
		m_bets.emplace_back(m_provider->requestInput());
	}
}
