#pragma once
#include "PLayer.h"
class NormalPlayer : public Player
{
public:
	NormalPlayer(const char* name , int money);
	~NormalPlayer();
	Action GetAction() const override;
};

