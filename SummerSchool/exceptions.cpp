#include "stdafx.h"
#include "exceptions.h"


FileNotFoundException::FileNotFoundException(const char * msg): std::invalid_argument(msg)
{}

InvalidSettingValue::InvalidSettingValue(std::string msg):m_msg(msg)
{}
