#pragma once

#include "Deck.h"
#include "Shoe.h"
#include "IInputProvider.h"
#include "IWinPolicy.h"

#include <vector>

class Player;

class Game
{
public:
	Game(size_t numPlayers, Player** players, IInputProvider* provider, IValidator* validator, IWinPolicy* winPolicy);
	Game(size_t numPlayers, Player** players);
	~Game();

	void Play();

private:
	void DealInitialCards();
	void GameLoop();
	void PrintPlayers() const;

	void Shuffle();
	void Deal(Player* player);

	void RequestBets();
private:
	size_t m_numPlayers;
	Player** m_players;
	std::vector<int> m_bets;

	Deck m_deck;
	Shoe m_shoe;

	IInputProvider* m_provider;
	IValidator* m_validator;
	IWinPolicy* m_winPolicy;
};

