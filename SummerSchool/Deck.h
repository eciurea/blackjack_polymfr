#pragma once

class Card;

class Deck
{
public:
	Deck();
	~Deck();

	size_t GetNumberOfCards() const;
	Card** GetCards() const;

private:
	Card** m_cards;
	size_t m_numCards;
};

