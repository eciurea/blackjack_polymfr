#pragma once
#include "Hand.h"

enum Action
{
	HIT,
	STAND
};

class Player
{
public:
	Player(const char* name);
	Player(const char* name, int currentAmountOfMoney );
	~Player();

	const char* GetName() const;
	Hand& GetHand();
	int GetCurrentAmountOfMoney() const;
	void AddMoney(int moneyToAdd);
	void SubstractMoney(int moneyToSubstract);

	virtual Action GetAction() const = 0;

protected:
	char* m_name;
	int m_currentAmountOfMoney;
	Hand m_hand;
};

