#include "stdafx.h"
#include "NormalPlayer.h"


NormalPlayer::NormalPlayer(const char * name ,int money):Player(name , money)
{}

NormalPlayer::~NormalPlayer()
{}


Action NormalPlayer::GetAction() const
{
	Action returnValue = STAND;
	if (m_hand.GetValue() < 19) returnValue = HIT;

	return returnValue;
}
